<?php
session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Mon site de E-commerce</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <script src="http://code.jquery.com/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <link href="css/style.css" rel="stylesheet" media="screen">
		<link rel="icon" type="image/png" href="img/favicon.ico" />
    </head>
    <body>
        <?php
            include 'include/header.php';
            include 'include/navigationBar.php';
        ?>
		<div id="main">
		<?php
		if(isset($_SESSION['active'])) //On a rien � faire ici, on retourne donc au panier				
		{

			echo	'<div class="alert alert-success">Merci pour votre achat.<a href="produit.php">Revenir sur la page des produits</a></div>';
						

						$nbrProduit = count($_SESSION['panier']['id']);
						
					for($i=0; $i<$nbrProduit; $i++)
					{
						$req = $dbc->prepare('INSERT INTO commandes (id_client, id_produit,quantite, id_transaction, date_creation) VALUES(:id_client,:id_produit, :quantite, :id_transaction, NOW())');
						$req->execute(array(
							'id_client' => $_SESSION['id'],
							'id_produit' => $_SESSION['panier']['id'][$i],
							'quantite' => $_SESSION['panier']['quantite'][$i],
							'id_transaction' => 0
							));	
					}
					session_destroy();
			}
			else
			{
				header('Location: produit.php');
			}
			echo '</div>';
			include ('include/footer.php');
		?>
		
	</body>
</html>