-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Lun 20 Janvier 2014 à 22:50
-- Version du serveur: 5.6.12-log
-- Version de PHP: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `bd_site_e`
--
CREATE DATABASE IF NOT EXISTS `bd_site_e` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `bd_site_e`;

-- --------------------------------------------------------

--
-- Structure de la table `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `id` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `mot_de_passe` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `date_creation` date DEFAULT NULL,
  `token` tinyint(1) NOT NULL DEFAULT '0',
  `adresse` varchar(255) DEFAULT NULL,
  `code_postal` int(4) NOT NULL DEFAULT '0',
  `ville` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `clients`
--

INSERT INTO `clients` (`id`, `nom`, `prenom`, `mot_de_passe`, `email`, `date_creation`, `token`, `adresse`, `code_postal`, `ville`) VALUES
('a', 'a', 'a', 'a', 'a', '2014-01-20', 0, NULL, 0, NULL),
('b', 'aa', 'a', 'a', 'a', '2014-01-20', 0, NULL, 0, NULL),
('d', 'd', 'd', 'd', 'd', '2014-01-20', 0, 'd', 0, ''),
('e', 'd', 'd', 'd', 'd', '2014-01-20', 0, 'd	d		', 0, 'dd'),
('HODOR?', 'HODOR', 'HODOR', 'HODOR', 'HODORHODOR', '2013-12-12', 0, 'Winterfell', 7330, ''),
('i', 's', 'd', 'd', 'd', '2014-01-20', 0, '', 0, ''),
('kevin.detrain', 'Detrain', 'Kevin', 'dc8b7f2576167dcb40865f79670123c0', 'blackhawk7333@hotmail.com', NULL, 1, 'cité wauters', 7333, 'tertre'),
('logan.detrain', 'Detrain', 'Detrain', '63d6baf65df6bdee8f32b332e0930669', 'toto@gmail.com', NULL, 1, '', 0, ''),
('salut', 'salut', 'salut', 'sa', 'sa', '2013-12-16', 0, '', 7777, '');

-- --------------------------------------------------------

--
-- Structure de la table `commandes`
--

CREATE TABLE IF NOT EXISTS `commandes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_client` varchar(255) NOT NULL,
  `id_produit` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  `id_transaction` int(11) DEFAULT NULL,
  `date_creation` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE IF NOT EXISTS `produit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `marque` varchar(255) NOT NULL,
  `categorie` varchar(255) NOT NULL,
  `prix` double NOT NULL,
  `image` text,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Contenu de la table `produit`
--

INSERT INTO `produit` (`id`, `nom`, `marque`, `categorie`, `prix`, `image`, `description`) VALUES
(1, 'EasyNote LM 86', 'Packard Bell', 'notebook', 427.99, 'img/ordinateurs/PB-LM86.jpg', ' Intel Core i5-430M à 2,26 GHz, mémoire vive : 4 Go, stockage : 500 Go Combiné lecteur graveur CD/DVD double couche biformat, écran 17,3" LED'),
(2, 'Satelite C660 1G2', 'Toshiba', 'notebook', 380, 'img/ordinateurs/Tosh-660-1G2.jpg', 'Intel Core i3-380M Processor Windows® 7 Home Premium 64-bit  Intel® Graphics Media Accelerator HD'),
(3, 'ultrabook U32U', 'ASUS', 'ultrabook', 500, NULL, NULL),
(4, 'RTTL88', 'Medion', 'ordinateur de bureau', 100, NULL, NULL),
(5, 'Mac 5', 'Mac', 'notebook', 700, NULL, NULL),
(6, 'cable réseau', 'divers', 'accessoires', 3, NULL, NULL),
(7, 'cable réseau droit 100 Metres ', 'divers', 'accessoires', 45, 'img/ordinateurs/FTP-0007.jpg', 'cable réseau droit 100 Metres  '),
(8, 'webcam', 'divers', 'accessoires', 10, NULL, NULL),
(9, 'azer', 'Dell', 'ultrabook', 700, NULL, NULL),
(10, 'letho', 'msi', 'notebook', 800, NULL, NULL),
(11, 'Aurore BB6-I7-16-M1H10-P7 Slim	\r\nLDLC Aurore BB6-I7-16-M1H10-P7 Slim', 'LDLC', 'notebook', 1163.41, 'img/ordinateurs/1.jpg', 'Intel Core i7-3632QM 16 Go SSD 120 Go + HDD 1 To 15.6" LED Graveur DVD Wi-Fi N/Bluetooth Webcam Windows 7 Professionnel 64 bits'),
(12, 'Bellone GB2-I7-16-S2', 'LDLC', 'notebook', 1385.99, 'img/ordinateurs/2.jpg', 'Intel Core i7-3630QM 16 Go SSD 240 Go 15.6" LED NVIDIA GeForce GTX 670M Lecteur Blu-ray/Graveur DVD Wi-Fi N/Bluetooth Webcam Windows 7 premium 64 bits'),
(13, 'Saturne GM1-I5-8-S1', 'LDLC', 'notebook', 1042.01, 'img/ordinateurs/3.jpg', 'Intel Core i5-4200M 8 Go SSD 180 Go 15.6" LED NVIDIA GeForce GTX 760M Lecteur Blu-ray/Graveur DVD Wi-Fi N/Bluetooth Webcam Windows 7 Professionnel 64 bits'),
(14, 'B43A-CU112G', 'ASUS', 'notebook', 858.69, 'img/ordinateurs/4.jpg', 'Intel Core i5-3320M 4 Go 500 Go 14" LED Graveur DVD Wi-Fi N/Bluetooth Webcam Windows 7 Professionnel 64 bits / Windows 8 Pro (garantie constructeur 1 an)'),
(15, 'G750JH-T4076H', 'ASUS', 'notebook', 1819.81, 'img/ordinateurs/LD0001299564_2_0001319733.jpg', 'Intel Core i7-4700HQ 16 Go 1.5 To (2x 750 Go) 17.3" LED NVIDIA GeForce GTX 780M Lecteur Blu-ray/Graveur DVD Wi-Fi N Webcam Windows 8 64 bits (garantie constructeur 1 an)'),
(16, 'ThinkCentre Edge 72 (RCCLCFR)', 'Lenovo', 'ordinateur de bureau', 393.55, 'img/ordinateurs/0001263984_0001313032_0001375882.jpg', 'Intel Pentium G2030 4 Go 500 Go Graveur DVD Windows 7 Professionnel 64 bits + Windows 8 Pro 64 bits (sans écran)'),
(17, 'ThinkStation E31 (SX4AKFR)', 'Lenovo', 'ordinateur de bureau', 971.19, 'img/ordinateurs/LD0001135794_2_0001135887.jpg', 'Intel Core i7-3770 4 Go 2 To Graveur DVD Windows 7 Professionnel 64 bits (sans écran)'),
(18, 'Aspire XC600 (DT.SLJEF.082)', 'Acer', 'ordinateur de bureau', 1010.68, 'img/ordinateurs/LD0001169026_2_0001169048_0001177539_0001372234.jpg', 'Intel Core i3-3240 4 Go 1 To Graveur DVD Windows 8 64 bits (sans écran)'),
(19, ' PC 1st Boss', 'LDLC', 'ordinateur de bureau', 364.4, 'img/ordinateurs/LD0001345740_2.jpg', 'AMD Athlon II X2 270 - Dual Core 4 Go 500 Go Radeon HD 7770 1 GB DDR3 DVD(+/-)RW DL (sans OS - non monté)'),
(20, 'LDLC PC eSport Elite', 'LDLC', 'ordinateur de bureau', 485.57, 'img/ordinateurs/LD0001314382_2.jpg', 'Intel Core i3 3220 4 Go 1 To NVIDIA GeForce GT 640 1 Go Graveur DVD - (sans OS - non monté)'),
(21, 'LDLC PC Guardian', 'LDLC', 'ordinateur de bureau', 606.97, 'img/ordinateurs/LD0001323320_2.jpg', 'AMD FX 4130 4 Go SSD 120 Go + HDD 1 To AMD Radeon HD 7770 1Go Graveur DVD (sans OS - non monté'),
(22, 'PC Forcer', 'LDLC', 'ordinateur de bureau', 809.31, 'img/ordinateurs/LD0001367729_2.jpg', 'Intel Core i5 4670K 8 Go 1 To NVIDIA GeForce GTX 660 2048 Mo Graveur DVD (sans OS - non monté)'),
(23, 'PC Major', 'LDLC', 'ordinateur de bureau', 586.74, 'img/ordinateurs/LD0001245715_2.jpg', 'Intel Core i5 4570 4 Go 1 To NVIDIA GeForce GTX 650 1 Go Graveur DVD (sans OS - non monté)'),
(24, 'BM6875-I53330076B', 'ASUS', 'ordinateur de bureau', 858.95, 'img/ordinateurs/LD0001204007_2_0001292063.jpg', 'Intel Core i5-3330 4 Go 1 To Graveur DVD Windows 8 Pro 64 bits (sans écran)'),
(25, 'Portégé R930-18W', 'Toshiba', 'notebook', 1565.92, 'img/ordinateurs/0001225862_0001290611.jpg', 'Intel Core i7-3540M 8 Go SSD 256 Go Go 13.3" LED Graveur DVD Wi-Fi N/Bluetooth/3G Webcam Windows 7 Professionnel 64 bits + DVD Windows 8 pro 64 bits'),
(26, 'Qosmio F750-12C', 'Toshiba', 'notebook', 1405.22, 'img/ordinateurs/LD0000959845_2.jpg', 'Intel Core i7-2670QM 8 Go 750 Go 15.6" LED 3D NVIDIA GeForce GT 540M Lecteur Blu-ray/Graveur DVD Wi-Fi N/Bluetooth Webcam Windows 7 Premium 64 bits (Emballage neutre*)'),
(27, 'Satellite C70-A-120', 'Toshiba', 'notebook', 374.09, 'img/ordinateurs/LD0001333990_2_0001334473.jpg', 'Intel Celeron 1005M 4 Go 500 Go 17.3" LED Graveur DVD Wi-Fi N/Bluetooth Webcam Windows 8 64 bits'),
(28, 'Satellite Pro L830-15M Noir', 'Toshiba', 'notebook', 605.99, 'img/ordinateurs/LD0001168761_2_0001285261_0001325988.jpg', 'Intel Core i3-3227U 4 Go 500 Go 13.3" LED Graveur DVD Wi-Fi N/Bluetooth Webcam Windows 7 Professionnel 64 bits + DVD Windows 8 Pro 64 bits'),
(29, 'Aspire S7-391-53334G12aws', 'Acer', 'ultrabook', 1213.04, 'img/ordinateurs/LD0001175030_2_0001175262_0001254544.jpg', 'Intel Core i5-3337U 4 Go SSD 128 Go 13.3" LED Tactile Wi-Fi N/Bluetooth Webcam Windows 8 64 bits'),
(30, 'TAICHI31-CX018H', 'ASUS', 'ultrabook', 1537.28, 'img/ordinateurs/169268_0001309702.jpg', 'Intel Core i7-3517U 4 Go SSD 256 Go 13.3" LED tactile + 13.3" LED Wi-Fi N/Bluetooth Webcam Windows 8 64 bits (garantie constructeur 1 an)'),
(31, 'Câble répétiteur USB 2.0', 'Logilink', 'accessoires', 39, 'img/ordinateurs/994017_BB_00_FB_EPS_1000.jpg', 'Caractéristiques techniques Type de câble USB 2.0 Couleur blanc Type de raccordement USB Blindage blindage partiel câble de données Type rallonge Connecteur A USB 2.0 A mâle AWG 26 Connecteur B USB 2.0 A femelle, USB 2.0 A femelle, USB 2.0 A femelle, USB 2.0 A femelle Type de câble USB 2.0'),
(32, 'COMPAQ CQ 2900EF ', 'HP', 'ordinateur de bureau', 359, 'img/ordinateurs/COM.jpg', ' AMD 1,4GHZ RAM 4GB DDR3 HDD 500GB DVD GRAVEUR WINDOWS 8'),
(33, 'PAVILION P6-2308EF ', 'HP', 'ordinateur de bureau ', 325, 'img/ordinateurs/hp-pavilion-p6-2393ef-pc.jpg', 'AMD Dual Core A4-5300 (3.4Ghz), 1Mo Cache L2, Socket FM2 Carte Mère : MSI MS7778, Chipset AMD A75 FCH Mémoire : 4Go DDR3 1333Mhz PC3-10 600 Mo/sec , 32Go Max (en 64bits) Disque Dur : 500 GB SATA 3G, 7200trs/mn Stockage Optique : DVD+/-RW SATA Double Couche LightScribe - Lecteur de cartes 15 en 1 Carte Graphique : AMD Radeon HD 7480D Intégrée, Ports : DVI-D et VGA Communications : Lan 10/100/1000 BT, Contrôleur Gigabit Ethernet Atheros AR8161L Son : Carte audio haute définition 8 canaux intégrée avec détection de jack, Codec IDT 92HD73E Divers : 2 Ports USB 2.0, 4 Ports USB 3.0, Clavier multimédia USB HP & Souris optique USB HP Système d''exploitation : Microsoft Windows 8 (64 bits) Modèle : C5V72EA Recertified HP Garantie Fabricant : 1 an (Pièces & main d''oeuvre, Pick up & return)');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
