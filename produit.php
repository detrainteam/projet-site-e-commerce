<?php
session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Mon site de E-commerce</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/style.css" rel="stylesheet" media="screen">
		<link rel="icon" type="image/png" href="img/favicon.ico" />
        <script src="http://code.jquery.com/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
        <?php
			include 'include/header.php';
            include 'include/navigationBar.php';
		?>
		<div id="main">
			<h1>Produits</h1>
			<?php
				/*
					Dans la condition suivante,
						-On cherche d'abord à savoir si $_POST['cat'] contient une valeur => L'utilisateur vient d'utiliser le formulaire de filtrage;
						-si la condition est toujours fausse, on cherche à savoir si $_GET['cat'] contient une valeur => L'utilisateur vient de changer de page en cliquant sur un lien après avoir filtrer;
						-si la condition est toujours fausse, on prend la valeur par défaut dans notre cas "Toute".
					cat contiendra le nom de la catégorie qui doit être affichée dans la page.
				*/
				$cat = (isset($_POST['cat'])) ? htmlspecialchars($_POST['cat']) : (!empty($_GET['cat']) ? htmlspecialchars($_GET['cat']) : "Toute");
				$marque = (isset($_POST['marque'])) ? htmlspecialchars($_POST['marque']) : (!empty($_GET['marque']) ? htmlspecialchars($_GET['marque']) : "Toute");

				//On essaye de recupérer le numero de la page precedente si il n'y en à pas, on utilise 0
				$pageActuelle = (empty($_GET['page'])) ? 0 : htmlspecialchars($_GET['page']);

				//Nombre d'articles qui seront affichés par page.
				$nbArticles = 12;
			?>
			<!-- Affichage des filtres -->
			<form method="post" action="produit.php" class="form-inline">
				<div class="form-group">
					<label class="sr-only" for="categorie">Catégories</label>
					<select class="form-control" name="cat">
						<option value="Toute" >Toute les catégories</option>
						<?php
							//on récupére toute les catégories
							$reponse = $dbc->query('SELECT DISTINCT categorie FROM produit') or die(print_r($dbc->errorInfo()));

							while($donnee = $reponse->fetch())
							{
								//On selectionne l'option qui à été utilisée pour filtrer dans la page précédente
								if($donnee['categorie']==$cat)
									echo '<option value="'. $donnee['categorie'] . '" selected>' . $donnee['categorie'] . '</option>';
								else
									echo '<option value="'. $donnee['categorie'] . '">' . $donnee['categorie'] . '</option>';
							}
							$reponse->closeCursor(); 
						?>
					</select>
				</div>
				<div class="form-group">
					<label class="sr-only" for="categorie">Marques</label>
					<select class="form-control" name="marque">
						<option value="Toute" >Toute les marques</option>
						<?php
							//On affiche uniquement les marques qui son présente dans la catégorie actuelle.
							if($cat != "Toute")
							{
								$reponse = $dbc->prepare('SELECT DISTINCT marque FROM produit where categorie = ?') or die(print_r($dbc->errorInfo()));
								$reponse->execute(array($cat));
							}
							else //on récupére toute les marques
								$reponse = $dbc->query('SELECT DISTINCT marque FROM produit') or die(print_r($dbc->errorInfo()));

							while($donnee = $reponse->fetch())
							{
								//On selectionne l'option qui à été utilisée pour filtrer dans la page précédente
								if($donnee['marque']==$marque)
									echo '<option value="'. $donnee['marque'] . '" selected>' . $donnee['marque'] . '</option>';
								else
									echo '<option value="'. $donnee['marque'] . '">' . $donnee['marque'] . '</option>';
							}
							$reponse->closeCursor();
						?>
					</select>
				</div>
				<button type="submit" class="btn btn-default">Filtrer</button>
			</form>
			<br/><br/>
			<!--Affichage des erreurs-->
			<div>
				<?php
					if(isset($_SESSION['erreur_produit']))
					{
						echo '<div class="alert alert-danger">'. $_SESSION['erreur_produit'] . '</div>';
						$_SESSION['erreur_produit'] = null;
					}	
				?>
			</div>
			<?php
				//						Récupération des données à afficher
				//==========================================================================
				$reponse = NULL;

				try
				{
					if($cat != "Toute" && $marque != "Toute") // On filtre grâce à la marque et la catégorie 
					{
						$reponse = $dbc->prepare('SELECT * FROM produit where categorie = ? AND marque = ? ORDER BY nom DESC LIMIT ' . ($nbArticles * $pageActuelle) . ', ' . $nbArticles) or die(print_r($dbc->errorInfo())); 
						$reponse->execute(array($cat, $marque));
					}
					else if($cat == "Toute" && $marque != "Toute") // On filtre grâce à la marque
					{
						$reponse = $dbc->prepare('SELECT * FROM produit where marque = ? ORDER BY marque DESC LIMIT ' . ($nbArticles * $pageActuelle) . ', ' . $nbArticles) or die(print_r($dbc->errorInfo())); 
						$reponse->execute(array($marque));
					}
					else if($cat != "Toute" && $marque == "Toute") // On filtre grâce à la catégorie 
					{
						$reponse = $dbc->prepare('SELECT * FROM produit where categorie = ? ORDER BY marque DESC LIMIT ' . ($nbArticles * $pageActuelle) . ', ' . $nbArticles) or die(print_r($dbc->errorInfo())); 
						$reponse->execute(array($cat));
					}
					else // On affiche tout
					{
						$reponse = $dbc->query('SELECT * FROM produit  ORDER BY categorie DESC LIMIT  ' . ($nbArticles * $pageActuelle) . ', ' . $nbArticles) or die(print_r($dbc->errorInfo()));
					}
				}
				catch(PDOException $e)
				{
					echo '<div class="alert alert-danger">Une erreur est survenue. Veuillez contacter le webmaster ou réessayer plus tard. Merci.</div>';
					exit();
				}
			?>
			<div class="annonce">
				<?php
					//						Affichage des vignettes
					//==========================================================================
					if(isset($reponse))
					{
						while($donnees=$reponse->fetch())
						{
							$image = ($donnees['image']==NULL) ? "img/notfound.png" : $donnees['image']; //S'il y existe une image pour notre article, on utilise son chemi si pas on utilise une image par defaut

							// La fonction php substr() permet de limité le nombre de caractère à afficher
							echo	'<div class="vignette">' .
										'<img src="' . $image. '" class="img-rounded" />' .
										'<a class="lien_detail" href="detail.php?produit=' . $donnees['id'] .' "><h3>' .  substr($donnees['nom'], 0, 20). '</h3></a>' . 
										'<p>Marque: ' . $donnees['marque'] .
										'<br>Prix: ' . $donnees['prix'] . ' €' .
										'<form method="post" action="addPanier.php">
											<input type="hidden" name="id" value="' . $donnees['id'] . '"/>
											<input type="hidden" name="adresse" value="produit.php">
											<input type="number" name="quantite" class="taille_text_box"/>
											<input type="submit" class="btn btn-default" value="Commander!"/>
										</form>
									</div>';
						}
						$reponse->closeCursor();
					}
				?>
			</div>
				<?php
					//								Pagination
					//==========================================================================
					$reponse = NULL;
					$nombre_liens = 0;

					try
					{
						// On récupére le nombre d'article total
						if($cat != "Toute" && $marque != "Toute")
						{
							$reponse = $dbc->prepare('SELECT COUNT(*) as total FROM produit where categorie = ? AND marque = ?');
							$reponse->execute(array($cat, $marque));
						}
						else if($cat == "Toute" && $marque != "Toute")
						{
							$reponse = $dbc->prepare('SELECT COUNT(*) as total FROM produit where marque = ?'); 
							$reponse->execute(array($marque));
						}
						else if($cat != "Toute" && $marque == "Toute")
						{
							$reponse = $dbc->prepare('SELECT COUNT(*) as total FROM produit where categorie = ?'); 
							$reponse->execute(array($cat));
						}
						else // On ne filtre pas.
							$reponse = $dbc->query('SELECT COUNT(*) total FROM produit');

						$donnee = $reponse->fetch();
						$nombre_liens = ceil($donnee['total'] / $nbArticles);

						$reponse->closeCursor();
					}
					catch(PDOException $e)
					{
						echo '<div class="alert alert-danger">Une erreur est survenue. Veuillez contacter le webmaster ou réessayer plus tard. Merci.</div>';
						exit();
					}

					//On affiche les liens
					if(isset($reponse) && $nombre_liens>0)
					{
						echo '<div id="liens_de_pagination">';

						//On affiche soit la 'case precedente' en grisé et non cliquable, soit la case cliquable
						if($pageActuelle == 0) //La case "precedente" doit être désactivée
							echo '<ul class="pagination"><li  class="disabled"><a>&laquo;</a></li>';
						else
						{
							//Si la page actuelle est > que 0, alors on peut passer a la page precedente si pas on reste sur la page actuelle
							$pagePrecedente = ($pageActuelle>0) ? $pageActuelle-1 : $pageActuelle;
							echo '<ul class="pagination"><li><a href="produit.php?page=' . $pagePrecedente . '&cat=' . $cat . '&marque=' . $marque . '">&laquo;</a></li>';
						}

						//On affiche les liens
						for($i=0; $i<$nombre_liens; $i++)
						{
							if($i==$pageActuelle)
								echo '<li class="active" ><a href="produit.php?page=' . $i . '&cat=' . $cat . '&marque=' . $marque . '">' . ($i+1) . '</a></li>';
							else
								echo '<li><a href="produit.php?page=' . $i . '&cat=' . $cat . '&marque=' . $marque . '">' . ($i+1) . '</a></li>';
						}

						//On affiche soit la 'case suivante' en grisé et non cliquable ,soit la case cliquable
						if(($pageActuelle+1) == $nombre_liens) //La case "suivante" doit être désactivée
							echo '<li  class="disabled"><a>&raquo;</a></li></ul>';
						else
						{
							//Si la page actuelle+1 est < que le nombre de liens, alors on peut passer a la page suivante si pas on reste sur la page actuelle
							$pageSuivante = ($pageActuelle+1 != $nombre_liens) ? $pageActuelle+1 : $pageActuelle;
							echo '<li><a href="produit.php?page=' . $pageSuivante . '&cat=' . $cat . '&marque=' . $marque . '">&raquo;</a></li></ul>';
						}

						echo '</div>';
					}
				?>
		</div>
			<?php
				include ('include/footer.php');
			?>
	</body>
</html>