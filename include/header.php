<?php
	require 'include/mysql.inc.php';
?>
<header>

	<div id="cadreUtilisateur">
	<!-- Liens -->
	<ul id="liensPratiques">
		<li><a href="contact.php">Contact</a></li>
		|
		<li><a href="inscription.php">Inscription</a></li>
		|
		<?php
		if(!isset($_SESSION['active']))
			echo '<li><a href="connexion.php">Connexion</a></li>';
		else
			echo '<li><a href="verification/verification_deconnexion.php">Deconnexion</a></li>';
		?>
	</ul>

	<!-- Cadre Utilisateur -->
	<?php
		if(isset($_SESSION['active']) && isset($_SESSION['id']))
			echo  "<p>Bonjour  ". $_SESSION['id'] .",</p>"; 

		$nbArticles = 0;

		//On compte le nombre d'articles
		if(isset($_SESSION['panier']['quantite']))
		{
			 for($i=0; $i<count($_SESSION['panier']['quantite']); $i++)
				$nbArticles += $_SESSION['panier']['quantite'][$i];
		}

		$textArticles = ($nbArticles>1) ? " articles " : " article ";
		
		echo '<p>Vous avez ' . $nbArticles . $textArticles .' dans <a href="panier.php">votre panier. <span class="glyphicon glyphicon-shopping-cart"></a></p>';
	?>
</div>
	<!-- Logo -->
	<a href="index.php"><img id="logo" src="img/logo.png"/></a>

	<!-- Barre de recherche -->
    <div id="divBarreDeRecherche">
        <form action="recherche.php" method="post" class="form-inline">
			<div class="form-group">
				<label class="sr-only" for="categorie">Catégorie</label>
				<select class="form-control" name="cat">
					<option value="Toute" >Toute</option>
					<?php
						try
						{
							$reponse = $dbc->query('SELECT DISTINCT categorie FROM produit ORDER BY categorie');
						
							while($donnee = $reponse->fetch())
							{
								echo '<option value="'. $donnee['categorie'] . '">' . $donnee['categorie'] . '</option>';
							}
						}
						catch (PDOException $e)
						{
							echo '<div class="alert alert-danger">Une erreur est survenue. Veuillez contacter le webmaster ou réessayer plus tard. Merci.</div>';
							exit();
						}
					?>
				</select>
			</div>
			<div class="form-group">
				<label class="sr-only" for="recherche">Recherche</label>
				<input type="text" class="form-control" name="recherche">
			</div>
			<button type="submit"  class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
		</form>
    </div>
</header>