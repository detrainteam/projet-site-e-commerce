<?php

define('USER', 'root');
define('MDP', '');
define('DNS','mysql:host=localhost;dbname=bd_site_e');

try {
    $dbc = new PDO(DNS, USER, MDP, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
    $dbc->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
 catch (PDOException $e){
     echo 'Erreur de connexion !!! :' . $e->getMessage() . '<br/>';
     exit();
 }
?>
