﻿<!DOCTYPE html>
<html>
    <head>
        <title>Mon site de E-commerce</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <link href="css/style.css" rel="stylesheet" media="screen">
            <!-- Bootstrap -->
            <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
			<link rel="icon" type="image/png" href="img/favicon.ico" />
            <script src="http://code.jquery.com/jquery.js"></script>
            <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
        <?php
            include 'include/header.php';
            include 'include/navigationBar.php';
        ?>
		<div id="main">
            <h1>Contact</h1>
			<table id="tableau_contact">
				<tr>
					<th class="titre_tableau">Téléphone: </th>
					<td>065\35.35.10</td>
				</tr>
				<tr>
					<th class="titre_tableau">E-mail: </th>
					<td><a href="mailto: info@ordimarkt.be">info@ordimarkt.be</a></td>
				</tr>
				<tr>
					<th class="titre_tableau_contact">Addresse: </th>
					<td id="adresse_contact">Boulevard du Roi Albert II, 27 B<br/>1030 Bruxelles<br/>Belgique</td>
				</tr>
			</table>
		</div>
		<?php
			include ('include/footer.php');
		?>
    </body>
</html>