<?php
	session_start();

	// on verifie que le contenu existe et qu'il s'agit bien d'un nombre
	if((isset($_POST['id']) && ctype_digit($_POST['id'])) && (isset($_POST['quantite']) && ctype_digit($_POST['quantite'])))
	{
		$idProduit = $_POST['id'];
		$quantite = $_POST['quantite'];
		$_SESSION['erreur_panier'] = NULL;

		if($quantite<=0)
		{
			$_SESSION['erreur_panier'] = 'Vous avez entrez une valeur incorrecte.';
			header('Location: panier.php');
			exit;
		}

		$positionProduit = array_search($idProduit, $_SESSION['panier']['id']);

		if($positionProduit !== FALSE)
		{
			$_SESSION['panier']['quantite'][$positionProduit] = $quantite;
		}
		else
		{
			$_SESSION['erreur_panier'] = 'Le produit n\'existe pas. Bizarre !';
		}

		header('Location: panier.php');
		exit;
	}
	else
	{
		$_SESSION['erreur_panier'] = 'Vous avez entrez autre chose qu\' un chiffre dans le champs quantite.';
		header('Location: panier.php');
		exit;
	}
?>