<?php 
	session_start();

	
	//on verifie que l'on a envoyer des valeur non vide, nulle ou non définie
	if(!empty($_POST['identifiant']) && !empty($_POST['mot_de_passe']))
	{
		require '../include/mysql.inc.php';
		try
		{
			$req = $dbc->prepare('SELECT  id,nom, prenom,mot_de_passe,token FROM clients WHERE id= ?');
			$req->execute(array(htmlspecialchars($_POST['identifiant'])));
			$donnee=$req->fetch();
		}
		catch (PDOException $e)
		{
			echo 'Une erreur s\'est produit lors de la phase de connexion au serveur, veuillez contacter le webmaster!!! <br/>';
			
			exit();
		}	
		
		// verification du mot de passe, en le cryptant pour voir s'il correspond au mot de passe déja crypté
		if($donnee != false && $donnee['mot_de_passe'] == hash_hmac('md5',htmlspecialchars($_POST['mot_de_passe']), 'secret'))
		{
			//on verifie que le client a bien valider son compte
			if($donnee['token'] == 1)
			{
				// on remet à zero le code d'erreur si tout va bien
				$_SESSION['erreur_connexion'] = "";
				//on envoye les valeurs pour que le client voit qu'il est bien connecté
				$_SESSION['id'] = htmlspecialchars($donnee['id']);
				$_SESSION['active'] = "on";
				header('location: ../index.php');
			}
			else
			{
				$_SESSION['erreur_connexion'] = "Vous n'avez pas encore valider votre adresse E-mail.";
				header('location: ../connexion.php');	
			}
		}
		else
		{
			$_SESSION['erreur_connexion'] = "Votre identifiant ou/et votre mot de passe est/sont probablement incorrecte(s).";
			header('location: ../connexion.php');	
		}

		$req->closeCursor();
	}
	else
	{
		$_SESSION['erreur_connexion'] = "Aucun de vos champs n'à été complété, recommencer sans modifier le comptenu de la page !!!";
		header('location: ../connexion.php');	
	}
?>
