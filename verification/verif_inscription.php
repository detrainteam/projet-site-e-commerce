<?php
	session_start();

		require '../include/mysql.inc.php';
		
	// on verifie que tout les champs on été complété	
	if(!empty($_POST['identifiant']) && !empty($_POST['nom']) && !empty($_POST['prenom']) && !empty($_POST['mot_de_passe']) && !empty($_POST['email']) && !empty($_POST['mot_de_passe_recopier']))
	{
		//on vérifie que le mot de passe correspond bien à son doublon
		if($_POST['mot_de_passe'] == $_POST['mot_de_passe_recopier'])
		{
			//test pour l'adresse e-mail
			$syntaxe_email='#^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]{2,}\.[a-zA-Z]{2,4}$#';
			if (preg_match($syntaxe_email, $_POST['email']))
			{
				try
				{
					$req=$dbc->prepare('SELECT id FROM clients WHERE id= ?');
					$req->EXECUTE(array(htmlspecialchars($_POST['identifiant'])));
					$donnee= $req->fetch();
					$req->closeCursor();
				}
				catch (PDOException $e)
				{
					echo 'Une erreur s\'est produit lors de la vérification de vos informations, veuillez contacter le webmaster!!! <br/>';
					exit();
				}	
					//on vérifie que l'identifiant n'existe pas déja dans la base de donnée(car il s'agit d'une clé primaire, elle doit être unique)
					//la fonction fetch() renvoye false lorsqu'il arrive à la fin ou qu'il ne contient rien
					if($donnee == false)
					{
						// le mot de passe est crypté
						$mot_de_passe_crypte = hash_hmac('md5', htmlspecialchars($_POST['mot_de_passe']) , 'secret');
					try
					{
						$req=$dbc->prepare('INSERT INTO  clients(id, nom, prenom, mot_de_passe, email, date_creation) VALUES (:id, :nom, :prenom, :mot_de_passe, :email, :date_creation)');					
						$req->EXECUTE(array('id'=> htmlspecialchars($_POST['identifiant']),
											'nom'=> htmlspecialchars($_POST['nom']),
											'prenom'=> htmlspecialchars($_POST['prenom']),
											'mot_de_passe'=> $mot_de_passe_crypte,
											'email'=> htmlspecialchars($_POST['email']),
											'date_creation'=> date("Y-m-d")
											));
					}
					catch (PDOException $e)
					{
						echo 'Une erreur s\'est produit lors de l\'envoye de vos informations, veuillez contacter le webmaster!!! <br/>';
						exit();
					}	
					//je crée un jeton à l'aide de l'identifiant du client pour permettre la création d'un jeton unique pour ce dernier.
					$jeton_de_validation = hash_hmac('md5',htmlspecialchars($_POST['identifiant']) , 'secret');
					//-------------------------------------------------------
					// destinataire
					 $to  =  htmlspecialchars($_POST['email']); // notez la virgule
					 // Sujet
					 $subject = 'Validation de votre compte sur OrdiMarkt';
					 // message
					 $message = '
					 <html>
						  <head>
								<title>Validation de votre compte sur OrdiMarkt</title>
						  </head>
						  <body>
								<p>bonjours monsieurs' . htmlspecialchars($_POST['nom']) . ',</p>
								<p>Nous vous demandons de suivre le lien suivant pour permettre la validation de votre compte.<br/>
								<a href="http://localhost/projet-site-e-commerce/validation_inscription.php?code='. $jeton_de_validation . '&&id='. htmlspecialchars($_POST['identifiant']).' ">lien de validation</a>
								</p>
						  </body>
						 </html> ';
						 // Pour envoyer un mail HTML, l'en-tête Content-type doit être défini
						 $headers  = 'MIME-Version: 1.0' . "\r\n";
						 $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
						 // En-têtes additionnels
						 $headers .= 'From:<kevindetrain@gmail.com>' . "\r\n";
						 // Envoi
						 mail($to, $subject, $message, $headers);
						//---------------------------------------------------
						$_SESSION['erreur_inscription'] = "";
						$_SESSION['confirmation_enregistrement'] ="Votre inscription s'est bien déroulé, veuillez suivre le lien se trouvant sur le mail de confirmation que vous avez reçu.<br/>
						Pour des raison de facilité voici votre lien => <a href='http://localhost/projet-site-e-commerce/validation_inscription.php?code=". $jeton_de_validation . "&&id=". htmlspecialchars($_POST["identifiant"])." '>lien de validation</a>";
						header('location: ../inscription.php');
					}
					else
					{
						$_SESSION['erreur_inscription'] = "L'identifiant existe déja." ;
						$_SESSION['confirmation_enregistrement'] ="";
						header('location: ../inscription.php');
					}
			}
			else
			{
				$_SESSION['confirmation_enregistrement'] ="";
				$_SESSION['erreur_inscription'] = "Votre adresse e-mail n'est pas écris dans un format comforme.";
				header('location: ../inscription.php');
			}
		}
		else
		{
			$_SESSION['confirmation_enregistrement'] ="";
			$_SESSION['erreur_inscription'] = "Votre mot de passe ne correspond pas à la vérification de mot de passe.";
			header('location: ../inscription.php');
		}
	}
	else
	{
		$_SESSION['confirmation_enregistrement'] ="";
		$_SESSION['erreur_inscription'] = "L'un de vos champs est vide !!!";
		header('location: ../inscription.php');
	}
?>