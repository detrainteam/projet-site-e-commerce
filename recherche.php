<?php
	session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Mon site de E-commerce</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link rel="icon" type="image/png" href="img/favicon.ico" />
        <script src="http://code.jquery.com/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <link href="css/style.css" rel="stylesheet" media="screen">
    </head>
    <body>
        <?php
            include 'include/header.php';
            include 'include/navigationBar.php';
        ?>
		<div id="main">
            <h1>Résultat de votre recherche</h1>
			<!--Affichage des erreurs-->
			<div>
				<?php
					if(isset($_SESSION['erreur_produit']))
					{
						echo '<div class="alert alert-danger">'. $_SESSION['erreur_produit'] . '</div>';
						$_SESSION['erreur_produit'] = null;
					}	
				?>
			</div>
            <?php
				/*
					Dans la contition suivante,
						-On cherche dabord à savoir si $_POST['cat'] contient une valeur => L'utilisateur vient d'utiliser le formulaire de filtrage.;
						-si la condition est toujours fausse, on cherche à savoir si $_GET['cat'] contient une valeur => L'utilisateur vient de changer de page en cliquant sur un lien après avoir filtrer;
						-si la condition est toujours fausse, on prend la valeur par défaut dans notre cas "Toute".
					cat contiendra le nom de la catégorie qui doit être affichée dans la page.
				*/
				$cat = (isset($_POST['cat'])) ? htmlspecialchars($_POST['cat']) : (!empty($_GET['cat']) ? htmlspecialchars($_GET['cat']) : "Toute");

				//On essaye de recupérer le numero de la page precedente si il n'y en à pas, on utilise 0
				$pageActuelle = (empty($_GET['page'])) ? 0 : htmlspecialchars($_GET['page']);

				$recherche = NULL;

				if(isset($_POST['recherche']))
					$recherche = htmlspecialchars($_POST['recherche']);
				else if(!empty($_GET['recherche']))
					$recherche = htmlspecialchars($_GET['recherche']);

				//Nombre d'articles qui seront affichés par page.
				$nbArticles = 12;
				try
				{
					if($cat!="Toute" && $recherche!=NULL)
					{
						$reponse = $dbc->prepare('SELECT * FROM produit WHERE categorie = ? AND nom LIKE \'%' . $recherche . '%\' ORDER BY nom DESC LIMIT ' . ($nbArticles * $pageActuelle) . ', ' . $nbArticles); 
						$reponse->execute(array(htmlspecialchars($_POST['cat'])));
					}
					elseif($cat!="Toute" && $recherche==NULL)
					{
						$reponse = $dbc->query('SELECT * FROM produit ORDER BY nom DESC LIMIT ' . ($nbArticles * $pageActuelle) . ', ' . $nbArticles); 
					}
					elseif($cat=="Toute" && $recherche!=NULL)
					{
						$reponse = $dbc->query('SELECT * FROM produit WHERE nom LIKE \'%' . $recherche . '%\' ORDER BY nom DESC LIMIT ' . ($nbArticles * $pageActuelle) . ', ' . $nbArticles); 
					}
					else
						$reponse = $dbc->query('SELECT * FROM produit WHERE nom LIKE \'%' . $recherche . '%\' ORDER BY nom DESC LIMIT ' . ($nbArticles * $pageActuelle) . ', ' . $nbArticles);
				}
				catch(PDOException $e)
				{
					echo '<div class="alert alert-danger">Une erreur est survenue. Veuillez contacter le webmaster ou réessayer plus tard. Merci.</div>';
					exit;
				}
            ?>

			<!-- Affichage des articles -->
			<div class="annonce">
				<?php
					$aucunResultat = TRUE;

					while($donnees=$reponse->fetch())
					{
						if($aucunResultat==TRUE)
						{
							//Si on rentre dans la cette boucle, cela veut dire qu'il y a un résultat à la recherche car $reponse->fetch() n'a pas retourné FALSE directement.
							$aucunResultat = FALSE;

							echo '<h3>Résultat(s) pour : ' . $recherche . '</h3>';
						}


						$image = ($donnees['image']==NULL) ? "img/notfound.png" : $donnees['image'];

							// La fonction php substr() permet de limité le nombre de caractère à afficher
							echo	'<div class="vignette">' .
										'<img src="' . $image. '" class="img-rounded" />' .
										'<a class="lien_detail" href="detail.php?produit=' . $donnees['id'] .' "><h3>' .  substr($donnees['nom'], 0, 20). '</h3></a>' . 
										'<p>Marque: ' . $donnees['marque'] .
										'<br>Prix: ' . $donnees['prix'] . ' €' .
										'<form method="post" action="addPanier.php">
											<input type="hidden" name="id" value="' . $donnees['id'] . '"/>
											<input type="hidden" name="adresse" value="recherche.php?recherche='.$recherche.'">
											<input type="number" name="quantite" class="taille_text_box"/>
											<input type="submit" class="btn btn-default" value="Commander!"/>
										</form>
									</div>';
					}

					if($aucunResultat)
						echo '<h3>Aucun résultat pour : ' . $recherche . '</h3>';
				?>
			</div>

			<!-- Pagination -->
			<div id="liens_de_pagination">
				<?php

					$reponse = NULL;
					$nombre_liens = 0;


					try
					{
						if($cat!="Toute")
						{
							$reponse = $dbc->prepare('SELECT COUNT(*) AS total FROM produit WHERE categorie = ? AND nom LIKE \'%' . $recherche . '%\'');
							$reponse->execute(array($cat));
						}
						else
							$reponse = $dbc->query('SELECT COUNT(*) AS total FROM produit WHERE nom LIKE \'%' . $recherche . '%\'');
													
						$donnee = $reponse->fetch();
						$nombre_liens = ceil($donnee['total'] / $nbArticles);
					}
					catch(PDOException $e)
					{
						echo '<div class="alert alert-danger">Une erreur est survenue. Veuillez contacter le webmaster ou réessayer plus tard. Merci.</div>';
						exit;
					}

					if($reponse!=NULL && $nombre_liens>0)
					{
						echo '<div id="liens_de_pagination">';

						//On affiche soit la 'case precedente' en grisé et non cliquable, soit la case cliquable
						if($pageActuelle == 0) //La case "precedente" doit être désactivée
							echo '<ul class="pagination"><li  class="disabled"><a>&laquo;</a></li>';
						else
						{
							//Si la page actuelle est > que 0, alors on peut passer a la page precedente si pas on reste sur la page actuelle
							$pagePrecedente = ($pageActuelle>0) ? $pageActuelle-1 : $pageActuelle;
							echo '<ul class="pagination"><li><a href="recherche.php?page=' . $pagePrecedente . '&cat=' . $cat . '&recherche=' . $recherche . '">&laquo;</a></li>';
						}

						//On affiche les liens
						for($i=0; $i<$nombre_liens; $i++)
						{
							if($i==$pageActuelle)
								echo '<li class="active" ><a href="recherche.php?page=' . $i . '&cat=' . $cat . '&recherche=' . $recherche . '">' . ($i+1) . '</a></li>';
							else
								echo '<li><a href="recherche.php?page=' . $i . '&cat=' . $cat . '&recherche=' . $recherche . '">' . ($i+1) . '</a></li>';
						}

						//On affiche soit la 'case suivante' en grisé et non cliquable ,soit la case cliquable
						if(($pageActuelle+1) == $nombre_liens) //La case "suivante" doit être désactivée
							echo '<li  class="disabled"><a>&raquo;</a></li></ul>';
						else
						{
							//Si la page actuelle+1 est < que le nombre de liens, alors on peut passer a la page suivante si pas on reste sur la page actuelle
							$pageSuivante = ($pageActuelle+1 != $nombre_liens) ? $pageActuelle+1 : $pageActuelle;
							echo '<li><a href="recherche.php?page=' . $pageSuivante . '&cat=' . $cat . '&recherche=' . $recherche . '">&raquo;</a></li></ul>';
						}
				
				
					echo '</div>';
					}

				?>

			</div>
		</div>
		<?php
			include ('include/footer.php');
		?>
	</body>
</html>