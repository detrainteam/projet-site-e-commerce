<?php
session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Mon site de E-commerce</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="css/style.css" rel="stylesheet" media="screen">
		<link rel="icon" type="image/png" href="img/favicon.ico" />
        <script src="http://code.jquery.com/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
        <?php
			include 'include/header.php';
            include 'include/navigationBar.php';
		?>
		<div id="main">
			<h1>Détail</h1>
			<?php
				
			if(isset($_GET['produit']))
			{

				$reponse = $dbc->prepare('SELECT * FROM produit where id = ?') or die(print_r($dbc->errorInfo())); 
				$reponse->execute(array($_GET['produit']));
				
				$donnees= $reponse->fetch();
				
					$image = ($donnees['image']==NULL) ? "img/notfound.png" : $donnees['image'];
				
				//affiche du titre + création du premier div compte l'image
				echo 	'<h3>' . $donnees['nom'] . '</h3>' .
							'<div id="cadre_detail_gauche">' . 
							'<img src="' . $image. '" class="img-rounded" />
							</div>';
				//création du cadre comptenant les détails			
				echo		'<div id="cadre_detail_droit">' .
							'<p>Catégorie: ' . $donnees['categorie'] . '<br/>' .
							'Marque: ' . $donnees['marque'] .
							'<br>Prix: ' . $donnees['prix'] . ' €<br/>' ;
							
				// on vérifie que l'on dispose bien d'une description pour l'article selectionner par le client 			
				if(isset($donnees['description']) && $donnees['description'] != null)
				{
					echo		'Detail: ' . $donnees['description'] .
								'</p>
								<form method="post" action="addPanier.php">
									<input type="hidden" name="id" value="' . $donnees['id'] . '"/>
									<input type="hidden" name="adresse" value="produit.php">
									<input type="number" name="quantite" class="taille_text_box"/>   
									<input type="submit" class="btn btn-default" value="Commander!"/>
								</form>
								</div>';
				}
				else
				{
					echo		'Detail: Il n\' y a pas de détail disponible concernant l\'article que vous avez selectionné, veuillez nous excuser '.
								'</p>
								<form method="post" action="addPanier.php">
									<input type="hidden" name="id" value="' . $donnees['id'] . '"/>
									<input type="hidden" name="adresse" value="produit.php">
									<input type="number" name="quantite" class="taille_text_box"/>   
									<input type="submit" class="btn btn-default" value="Commander!"/>
								</form>
								</div>';
				}
			}		

			?>

		</div>
			<?php
				include ('include/footer.php');
			?>
	</body>
</html>