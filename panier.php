<?php
session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Mon site de E-commerce</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link rel="icon" type="image/png" href="img/favicon.ico" />
        <script src="http://code.jquery.com/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <link href="css/style.css" rel="stylesheet" media="screen">
    </head>
    <body>
        <?php
            include 'include/header.php';
            include 'include/navigationBar.php';
        ?>
		<div id="main">
		<!--Affichage des erreurs-->
		<h1>Panier</h1>
		<div>
			<?php
				if(!empty($_SESSION['erreur_panier']))
				{	echo '<div class="alert alert-danger">'. $_SESSION['erreur_panier'] . '</div>';
					$_SESSION['erreur_panier'] = null;
				}	
			?>
		</div>
				<?php
					if(isset($_SESSION['panier']))
					{
						$nbrElements = count($_SESSION['panier']['id']);

						if($nbrElements>0)
						{

						echo '<table class="table">
							<tr>
								<th>Article</th>
								<th>Nom</th>
								<th>N° de produit</th>
								<th>Prix unitaire</th>
								<th>Quantité</th>
								<th>Prix</th>
								<th>Actions</th>
							</tr>';

							$total=0;
							$nbrProduit = count($_SESSION['panier']['id']);

							for($i=0; $i<$nbrProduit; $i++)
							{
								//On recupére l'id et la quantite de l'éléments que l'on doit afficher.
								$id = $_SESSION['panier']['id'][$i];
								$quantite = $_SESSION['panier']['quantite'][$i];

								//On recupére les informations des produits.
								$produits = $dbc->prepare('SELECT * FROM produit WHERE id=?');
								$produits->execute(array($id));

								foreach($produits as $produit) //En principe, on a un seul éléments dans ce tableau sauf si l'id se répéte dans la bdd ce qui ne doit pas arriver.
								{
									//S'il y existe une image pour notre article, on utilise son chemi si pas on utilise une image par defaut
									$image = ($produit['image']==NULL) ? "img/notfound.png" : $produit['image'];

									if($i%2==0)
										echo 	'<tr class="ligneColorie">';
									else
										echo 	'<tr>';

									echo    '
												<td><img src="' . $image . '" class="imagePanier"/></td>
												<td>' . $produit['nom'] . '</td>
												<td>' . $id . '</td>
												<td>' . $produit['prix'] . ' € ' . '</td>
												<td>
													<form method="post" action="editPanier.php" class="bouton_actions">
														<input type="hidden" name="id" value="' . $id . '"/>
														<input type="number" name="quantite" value="' . $quantite . '"></input>
														<button type="submit" class="actionPanier"><span class="glyphicon glyphicon-edit"></span></button>
													</form>
												</td>
												<td>' . ($produit['prix'] * $quantite) . ' € ' . '</td>
												<td>
													<form method="post" action="remPanier.php" class="bouton_actions">
														<input type="hidden" name="id" value="' . $id . '">
														<button type="submit"><span class="glyphicon glyphicon-trash"></span></button>
													</form>
												</td>
											</tr>';

									$total += $produit['prix']  * $quantite;
								}
							}
							echo '</table>';
							echo	'<div>Total: ' . $total . ' € </div>';
						}
						else
						{
							echo '<p>Votre panier est vide</p>';
						}
					}
					else
					{
						echo '<p>Votre panier est vide</p>';
					}
				?>
			</table>
			<form methode="POST" action="produit.php" class="bouton_actions">
				<input type="submit"  class="btn btn-default" value="Poursuivre vos achats"/>
			</form>
			<?php
				if(isset($_SESSION['active']))
				{
					$disactive = (empty($_SESSION['panier']['id'])) ? "disabled" : "";

					echo '<form methode="POST" action="commande.php" class="bouton_actions">
							<input type="submit" '. $disactive .' class="btn btn-default" value="Passer commande"/>
						</form>';

				}
				else
				{
					echo '<a href="connexion.php"><button class="btn btn-default" >Connectez-vous pour passer commande</button></a>';
				}

			?>
		</div>
		<?php
			include ('include/footer.php');
		?>
	</body>
</html>