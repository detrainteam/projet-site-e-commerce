<?php
session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Mon site de E-commerce</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <script src="http://code.jquery.com/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
		<link href="css/style.css" rel="stylesheet" media="screen">
		<link rel="icon" type="image/png" href="img/favicon.ico" />
    </head>
    <body>
        <?php
            include ('include/header.php');
            include ('include/navigationBar.php');
        ?>
		<div id="main">
			<h1>Connexion</h1>
					<?php
						include ('include/connexion.php');
					?>
				</form>
			</div>
		</div>
		<?php
			include ('include/footer.php');
		?>
    </body>
</html>