<?php
session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Mon site de E-commerce</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <script src="http://code.jquery.com/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <link href="css/style.css" rel="stylesheet" media="screen">
		<link rel="icon" type="image/png" href="img/favicon.ico" />
	</head>
    <body>
        <?php
            include 'include/header.php';
            include 'include/navigationBar.php';
        ?>
		<div id="main">
		<!--Affichage des erreurs-->
		<div>
			<?php
				if(!isset($_SESSION['active'])) //On a rien à faire ici, on retourne donc au panier
					header('Location: panier.php');

				if(!empty($_SESSION['erreur_panier']))
				{	echo '<div class="alert alert-danger">'. $_SESSION['erreur_panier'] . '</div>';
					$_SESSION['erreur_panier'] = null;
				}

				if(!empty($_GET['payment']) && $_GET['payment']==1)
					echo '<div class="alert alert-danger">Payement annulé :-/</div>';
			?>
		</div>
		<h1>Commande</h1>
			<?php
				if(isset($_SESSION['panier']))
				{
					echo '<table class="table">
							<tr>
								<th>Article</th>
								<th>Nom</th>
								<th>N° de produit</th>
								<th>Prix unitaire</th>
								<th>Quantité</th>
								<th>Prix</th>
							</tr>';

					$nbrElements = count($_SESSION['panier']['id']);

					if($nbrElements>0)
					{
						$total=0;
						$nbrProduit = count($_SESSION['panier']['id']);

						for($i=0; $i<$nbrProduit; $i++)
						{
							//On recupére l'id et la quantite de l'élément que l'on doit afficher.
							$id = $_SESSION['panier']['id'][$i];
							$quantite = $_SESSION['panier']['quantite'][$i];

							//On recupére les informations du produit.
							$produits = $dbc->prepare('SELECT * FROM produit WHERE id=?');
							$produits->execute(array($id));

							foreach($produits as $produit) //En principe, on a un seul élément dans ce tableau sauf si l'id se répéte dans la bdd ce qui ne doit pas arriver.
							{
								//S'il existe une image pour notre article, on utilise son chemin si pas on utilise une image par defaut
								$image = ($produit['image']==NULL) ? "img/notfound.png" : $produit['image'];

								if($i%2==0)
									echo 	'<tr class="ligneColorie">';
								else
									echo 	'<tr>';

								echo    '
											<td><img src="' . $image . '" class="imagePanier"/></td>
											<td>' . $produit['nom'] . '</td>
											<td>' . $id . '</td>
											<td>' . $produit['prix'] . ' € ' . '</td>
											<td>' . $quantite . '</td>
											<td>' . ($produit['prix'] * $quantite) . ' € ' . '</td>
										</tr>';

								$total += $produit['prix']  * $quantite;
							}
						}
						echo '</table>';
						echo	'<div>Total: ' . $total . ' € </div>';

						echo	'<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
									<input type="hidden" name="cmd" value="_cart"/>
									<input type="hidden" name="upload" value="1"/>
									<input type="hidden" name="business" value="logan.detrain-facilitator@gmail.com"/>
									<input type="hidden" name="currency_code" value="EUR"/>
									<input type="hidden" name="image_url" value="http://imageshack.com/a/img855/584/3ljc.png"/>
									<input type="hidden" name="return" value="http://localhost/projet-site-e-commerce/succes.php?commande=0"/>
									<input type="hidden" name="cbt" value="Revenir sur Ordimarkt"/>
									<input type="hidden" name="shopping_url" value="http://localhost/projet-site-e-commerce/produit.php"/>
									<input type="hidden" name="cancel_return" value="http://localhost/projet-site-e-commerce/commande.php?payment=1"/>
									<input type="hidden" name="notify_url" value="http://localhost/projet-site-e-commerce/succes.php"/>';

									for($i=0; $i<$nbrProduit; $i++)
									{
										//On recupére l'id et la quantite de l'élément que l'on doit afficher.
										$id = $_SESSION['panier']['id'][$i];
										$quantite = $_SESSION['panier']['quantite'][$i];

										//On recupére les informations du produit.
										$produits = $dbc->prepare('SELECT * FROM produit WHERE id=?');
										$produits->execute(array($id));

										foreach($produits as $produit) //En principe, on a un seul élément dans ce tableau sauf si l'id se répéte dans la bdd ce qui ne doit pas arriver.
										{
											echo '<input type="hidden" name="item_number_'	. ($i+1) . '" value="'. $id .'"/>';
											echo '<input type="hidden" name="item_name_'	. ($i+1) . '" value="' . $produit['nom'] . '"/>';
											echo '<input type="hidden" name="amount_'		. ($i+1) . '" value="'. $produit['prix'] .'"/>';
											echo '<input type="hidden" name="quantity_'		. ($i+1) . '" value="'. $quantite .'"/>';
											echo '<input type="hidden" name="tax_rate_'		. ($i+1) . '" value="0.21"/>';
										}
									}
						
						echo	'<input type="submit" value="Payer avec PayPal"></form>';
					}
					else
					{
						echo '<p>Votre panier est vide</p>';
					}
				}
				else
				{
						echo '<p>Votre panier est vide</p>';
				}
			?>
		</div>
		<?php
			include ('include/footer.php');
		?>
	</body>
</html>