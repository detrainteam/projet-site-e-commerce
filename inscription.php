<?php
session_start();
?>
<!DOCTYPE html>
    <head>
        <title>Mon site de E-commerce</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <script src="http://code.jquery.com/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
		<link href="css/style.css" rel="stylesheet" media="screen">
		<link rel="icon" type="image/png" href="img/favicon.ico" />
    </head>
    <body>
        <?php
            include 'include/header.php';
            include 'include/navigationBar.php';
        ?>
	<div id="main">
		<h1>Inscription</h1>
			<?php
				if(!empty($_SESSION['erreur_inscription']))
				{
					echo '<div style=" color: rgb(210, 0, 0); width:400px;">';
					echo $_SESSION['erreur_inscription'];
					$_SESSION['erreur_inscription'] = null;
					echo '</div>';
				}
				if(!empty($_SESSION['confirmation_enregistrement']))
				{	echo '<div style=" color: rgb(0, 210, 0);width:400px;">';
					echo $_SESSION['confirmation_enregistrement'];
					$_SESSION['confirmation_enregistrement'] = null;
					echo '</div>';
				}
			
			?>
			<div  class="inscription">
			<h2>Inscrivez-vous</h2>
				<form method="post" action="verification/verif_inscription.php">
					<table id="tab_inscription"> 
						<tr> 
								<th class="inscr_col_text">Identifiant :</th> 
								<td class="inscr_col_entree"><input type="text" name="identifiant" required = "required" value="" placeholder="Identifiant"/></td>
						</tr> 
						<tr> 
								<th class="inscr_col_text">Nom :</th> 
								<td class="inscr_col_entree"><input type="text"  name="nom" value=""  required = "required"/></td>
						</tr> 
						<tr> 
								<th class="inscr_col_text">Prénom :</th> 
								<td class="inscr_col_entree"><input type="text"  name="prenom" value=""  required = "required"/></td>
						</tr> 
						<tr> 
								<th class="inscr_col_text">Mot de passe :</th> 
								<td class="inscr_col_entree"><input type="password"  name="mot_de_passe" value=""  required = "required"/></td>			
						</tr> 
						<tr> 
								<th class="inscr_col_text">Recopier votre mot de passe :</th> 
								<td class="inscr_col_entree"><input type="password"  name="mot_de_passe_recopier"  required = "required" value=""/></td>			
						</tr> 
						<tr> 
								<th class="inscr_col_text">Adresse e-mail :</th> 
								<td class="inscr_col_entree"><input type="email"  name="email"  required = "required"/></td>			
						</tr> 
						<tr> 
								<td colspan="2" class="col_valider"><input type="submit" class="btn" value="Inscription"/></td>
						</tr> 
					</table> 
				</form>
			</div>
			
			<?php
			// partie connexion
			if(!isset($_SESSION['active']))
			{ ?>
			<div id="replique_cadre_connexion">
				<h3>Vous possédez déjà un compte ?</h3>
				<?php
					include ('include/connexion.php');
				?>
			</div>
			<?php 
			} ?>
			
	</div>
		<?php
			include ('include/footer.php');
		?>
    </body>
</html>