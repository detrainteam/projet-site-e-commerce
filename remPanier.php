<?php
	session_start();

	if(isset($_POST['id']))
	{
		$id = $_POST['id'];

		$temp['panier'] = array();
		$temp['panier']['id'] = array();
		$temp['panier']['quantite'] = array();

		$nbrArticles = count($_SESSION['panier']['id']);

		for($i=0; $i<$nbrArticles; $i++)
		{
			if($_SESSION['panier']['id'][$i] != $id)
			{
				array_push($temp['panier']['id'], $_SESSION['panier']['id'][$i]);
				array_push($temp['panier']['quantite'], $_SESSION['panier']['quantite'][$i]);
			}
		}

		$_SESSION['panier'] = $temp['panier'];
	}

	header('Location: panier.php');
	exit;
?>