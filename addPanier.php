<?php
	session_start();

	// on verifie que le contenu existe et qu'il s'agit bien d'un nombre
	if((isset($_POST['id']) && ctype_digit($_POST['quantite'])) && (isset($_POST['quantite']) && ctype_digit($_POST['quantite'])))
	{
		$idProduit = $_POST['id'];
		$quantite = $_POST['quantite'];
		$_SESSION['erreur_produit'] = NULL;
		
		//Création du panier
		if(!isset($_SESSION['panier']))
		{
			$_SESSION['panier'] = array();
			$_SESSION['panier']['id'] = array();
			$_SESSION['panier']['quantite'] = array();
		}

		if($quantite<=0)
		{
					$_SESSION['erreur_produit'] = 'Vous avez entré une valeur incorrecte.';
					if($_POST["adresse"] == 'produit.php')
						header('Location: produit.php');
					elseif($_POST["adresse"] == 'recherche.php')
						header('Location: produit.php');	
					else
						header('Location: index.php');
					exit;
		}

		$positionProduit = array_search($idProduit, $_SESSION['panier']['id']);

		if($positionProduit !== FALSE)
		{
			$_SESSION['panier']['quantite'][$positionProduit] += $quantite;
		}
		else
		{
			array_push($_SESSION['panier']['id'], $idProduit);
			array_push($_SESSION['panier']['quantite'], $quantite);
		}
		header('Location: panier.php');
		exit;
	}
	else
	{
		$_SESSION['erreur_produit'] = 'Vous avez entré autre chose qu\' un chiffre dans le champs quantite.';

		//on redirige le client vers la page où l'erreur a été commise.
		if(isset($_POST["adresse"] ))
			header('Location: ' . $_POST["adresse"]);
		else
			header('Location: index.php');

		exit;
	}

?>