<?php
session_start();
?>
<!DOCTYPE html>
    <head>
        <title>Mon site de E-commerce</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <script src="http://code.jquery.com/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
		<link href="css/style.css" rel="stylesheet" media="screen">
		<link rel="icon" type="image/png" href="img/favicon.ico" />
    </head>
    <body>
        <?php
            include 'include/header.php';
            include 'include/navigationBar.php';
        ?>
	<div id="main">
		<h1>Validation de l'inscription</h1>
		<?php
			//on vérifie que l'on reçoit bient des variables à l'aide de l'url
			if(isset($_GET['id']) && isset($_GET['code']))
			{
			//on crypte l'identifant reçu pour vérifier par la suite, si il correspond bien au code reçu ( le code reçu est en fait le cryptage de l'id que l'on a réalisé lors de l'inscription)
			$cryptage_id=hash_hmac('md5',htmlspecialchars($_GET['id']) , 'secret');
				if($cryptage_id == htmlspecialchars($_GET['code']))
				{	
					try
					{
						$req=$dbc->prepare('UPDATE clients SET token = 1 WHERE id=?');
						$req->execute(array(htmlspecialchars($_GET['id'])));
						echo "Votre inscription a été validé!!";
					}
					catch (PDOException $e)
					{
						echo 'Une erreur s\'est produit lors de la phase de vérification de votre compte, veuillez contacter le webmaster!!! <br/>';
						exit();
					}	
				}
				else
				{
					echo "les arguments se trouvant dans le lien sont incorrects";
				}
			 
			}
			else
			{
			echo "certain arguments se trouvant dans le lien sont manquant";
			}
		
		?>
	</div>
		<?php
			include ('include/footer.php');
		?>
    </body>
</html>