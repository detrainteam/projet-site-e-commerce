<?php
session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Mon site de E-commerce</title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
        <script src="http://code.jquery.com/jquery.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <link href="css/style.css" rel="stylesheet" media="screen">
		<link rel="icon" type="image/png" href="img/favicon.ico" />
    </head>
    <body>
        <?php
            include 'include/header.php';
            include 'include/navigationBar.php';
        ?>
		<div id="main">
            <h1>Accueil</h1>
				<?php
				//-----Affichage des erreurs--------
					if(isset($_SESSION['erreur_produit']))
					{
						echo '<div class="alert alert-danger">'. $_SESSION['erreur_produit'] . '</div>';
						$_SESSION['erreur_produit'] = null;
					}	
			?>
			<h2>Nos 3 dernières nouveautés :</h2>
			<div class="annonce">
				<?php
					//--affichage des produits--
					try
					{
						$req=$dbc->query('SELECT * FROM produit ORDER BY id DESC	LIMIT 0, 3');

						while($donnees=$req->fetch())
						{
							$image = ($donnees['image']==NULL) ? "img/notfound.png" : $donnees['image'];
							
							// la fonction php substr() permet de limité le nombre de caractère à afficher
							echo '<div class="vignette">' .
								'<img src="' . $image. '" class="img-rounded" />' .
								'<a class="lien_detail" href="detail.php?produit=' . $donnees['id'] .' "><h3>' .  substr($donnees['nom'], 0, 20). 
								'</h3></a>' . '<p>Marque: ' . $donnees['marque'] .'<br>Prix: ' . $donnees['prix'] . ' €' .
								'<form method="post" action="addPanier.php">
									<input type="hidden" name="id" value="' . $donnees['id'] . '">
									<input type="hidden" name="adresse" value="index.php">
									<input type="number" name="quantite" class="taille_text_box">
									<input type="submit" class="btn btn-default" value="Commander!"/>
								</form></div>';
						}
					}
					catch(PDOException $e)
					{
						echo '<div class="alert alert-danger"> Une erreur s\'est produite sur la page lors de l\'accès à la base de données, 
						veuillez contacter le webmaster responsable de ce site. </div>';
					}

				?>
			</div>
		</div>
		<?php
			include ('include/footer.php');
		?>
	</body>
</html>